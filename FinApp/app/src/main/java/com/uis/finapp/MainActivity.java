package com.uis.finapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText user,password;
    private TextView nombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user = (EditText) findViewById(R.id.textuser);
        password = (EditText) findViewById(R.id.textclave);
        nombre = (TextView) findViewById(R.id.LEF);

    }

    public void ingresar(View view){
        Intent i = new Intent(this, Finapp2.class);
        startActivity(i);
    }
}