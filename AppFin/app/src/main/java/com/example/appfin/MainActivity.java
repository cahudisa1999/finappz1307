package com.example.appfin;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private TextView txtUsuario;
    private TextView txtPassword;
    private String txtNombre;
    private String txtApellido;
    private String UsuarioTabla;
    private String ClaveTabla;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtUsuario = (TextView) findViewById(R.id.TxtUsuario);
        txtPassword = (TextView) findViewById(R.id.TxtPassword);

        txtApellido="";
        txtNombre="";
        UsuarioTabla="";
        ClaveTabla="";
    }

    public void loguear(View view){
        Intent intent = new Intent(this,Registrar.class);
        startActivity(intent);
    }

    public void BuscarUsuario(View view){

        AdminSQL admin = new AdminSQL(this, "Entidades", null, 1);
        SQLiteDatabase DB = admin.getWritableDatabase();

        String correo = txtUsuario.getText().toString();

        //Toast.makeText(this,"Usuario no existe"+correo,Toast.LENGTH_LONG).show();

        if(!correo.isEmpty()){

            Cursor fila = DB.rawQuery("Select nombre,apellido,correo,clave" +
                    " from usuarios where correo ='"+ correo+"'",null);

            if(fila.moveToFirst()){
                txtNombre=fila.getString(0);
                txtApellido=fila.getString(1);
                UsuarioTabla= fila.getString(2);
                ClaveTabla = fila.getString(3);

                Toast.makeText(this,"Clave " +ClaveTabla+" "+txtPassword.getText(),Toast.LENGTH_LONG).show();
                DB.close();

                if(ClaveTabla!=txtPassword.getText()){
                    Intent intent = new Intent(this,MenuOpciones.class);
                    startActivity(intent);
                }else
                {
                    AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
                    dialogo.setTitle("Alerta");
                    dialogo.setMessage("Clave Errada");

                    dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    dialogo.show();
                   // Toast.makeText(this,"Clave Incorrecta " ,Toast.LENGTH_LONG).show();
                }
                //Toast.makeText(this,"Usuario Si existe  " ,Toast.LENGTH_LONG).show();

            }else{
                Toast.makeText(this,"Usuario no existe",Toast.LENGTH_LONG).show();

            }
        }else{
            Toast.makeText(this,"el campo Usuario no puede ser vacio",Toast.LENGTH_LONG).show();
        }

    }
}