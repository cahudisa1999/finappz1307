package com.example.appfin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.appfin.ui.main.MapasFragment;

public class Mapas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapas);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, MapasFragment.newInstance())
                    .commitNow();
        }
    }
}