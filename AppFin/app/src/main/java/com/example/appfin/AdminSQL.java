package com.example.appfin;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class AdminSQL extends SQLiteOpenHelper {

    public AdminSQL(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void  onCreate(SQLiteDatabase baseDatos){
        baseDatos.execSQL("create table usuarios ("+"correo text primary key ,nombre text,apellido text,clave text)");
        baseDatos.execSQL("create table lugares ("+"tipolugar text primary key ,entidad text,franquicia text,direccion text,telefono text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
