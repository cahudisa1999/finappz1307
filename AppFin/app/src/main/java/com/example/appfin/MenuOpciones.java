package com.example.appfin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MenuOpciones extends AppCompatActivity {

    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_opciones);
    }

    public void retorna(View view){
        finish();
    }

    public void BuscarEntidad(View view){
        Intent intent = new Intent(this,buscar.class);
        startActivity(intent);
    }

    public void registrarEntidad(View view){
        Intent intent = new Intent(this,registrarentidad.class);
        startActivity(intent);
    }
}