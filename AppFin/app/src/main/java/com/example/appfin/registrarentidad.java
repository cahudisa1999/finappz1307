package com.example.appfin;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

public class registrarentidad extends AppCompatActivity {

    private Spinner spinnerEnt;
    private Spinner spinnerFran;

    private RadioButton rbOficina;
    private RadioButton rbCajero;
    private RadioButton rbCorresponsal;

    private EditText txtDireccion;
    private EditText txtTelefono;


    String [] opcionesENT = {"Bancolombia","Davivienda","Scotiabank","Bancamia","Sudameris","Banco de Bogota","BBVA","Banco de Occidente","Bsnco ITAU","Banco Agrario","Banco Popular","Caja Social","AV Villas","Banco Union","Sudameris","Banco Falabella","Banco Pichincha","Banco Finandina","Sudameris"};
    String [] opcionesFRAN = {"VISA","MASTERCARD","AMERICAN EXPRESS"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarentidad);

        spinnerEnt= (Spinner)  findViewById(R.id.spinnerENT);
        spinnerFran= (Spinner)  findViewById(R.id.spinnerFRAN);

        txtDireccion= (EditText)  findViewById(R.id.txtDireccionSuc);
        txtTelefono= (EditText)  findViewById(R.id.txtTelefonoSuc);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,opcionesENT);
        spinnerEnt.setAdapter(adapter);

        ArrayAdapter <String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,opcionesFRAN);
        spinnerFran.setAdapter(adapter2);

        rbOficina=(RadioButton) findViewById(R.id.RBOficina);
        rbCajero=(RadioButton) findViewById(R.id.RBCajero);
        rbCorresponsal=(RadioButton) findViewById(R.id.RBCorresponsal);

    }

    public void retornaENT(View view){
        finish();
    }

    public void obtieneDatos(View view){

        String OpcEntidad= spinnerEnt.getSelectedItem().toString();
        String OpcFranquicia= spinnerFran.getSelectedItem().toString();
        String TipoEntidad="";
        String Direccion = txtDireccion.getText().toString();
        String Telefono = txtTelefono.getText().toString();


        if (!OpcEntidad.isEmpty() &&  !OpcFranquicia.isEmpty()){

            if (rbOficina.isChecked()==true){
                TipoEntidad="Oficina";
            }
            else{if (rbCajero.isChecked()==true){
                TipoEntidad="Cajero";
                 }
                 else{
                     if(rbCorresponsal.isChecked()==true){
                        TipoEntidad="Corresponsal";

                     }
                     else{
                         Toast.makeText(this, "Debe seleccionar un tipo de entidad", Toast.LENGTH_LONG).show();
                     }
                 }
             }
            if(Direccion.isEmpty() && Telefono.isEmpty()){
                Toast.makeText(this, "Los Campos direccion y telefono son mandatorios", Toast.LENGTH_LONG).show();
            }
            else{
                // Grabar datos
                AdminSQL admin = new AdminSQL(this, "Entidades", null, 1);
                SQLiteDatabase DB = admin.getWritableDatabase();

                ContentValues registro = new ContentValues();
                registro.put("tipoentidad", TipoEntidad);
                registro.put("entidad", OpcEntidad);
                registro.put("franquicia", OpcFranquicia);
                registro.put("direccion", Direccion);
                registro.put("telefono", Telefono);

                DB.insert("lugares", null, registro);

                DB.close();

                AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
                dialogo.setTitle("Informacion");
                dialogo.setMessage("Registro de Lugar guardado Exitosamente");

                dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                dialogo.show();

            }
        }
        else{
            Toast.makeText(this, "Los datos no pueden ser nulos", Toast.LENGTH_LONG).show();
        }

    }
}