package com.example.appfin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class buscar extends AppCompatActivity {


    private Spinner spinnerban;

    String [] opcionesBAN = {"Bancolombia","Davivienda","Scotiabank","Bancamia","Sudameris","Banco de Bogota","BBVA","Banco de Occidente","Bsnco ITAU","Banco Agrario","Banco Popular","Caja Social","AV Villas","Banco Union","Sudameris","Banco Falabella","Banco Pichincha","Banco Finandina","Sudameris"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar);

        spinnerban= (Spinner)  findViewById(R.id.spinnerBan);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,opcionesBAN);
        spinnerban.setAdapter(adapter);


    }

    public void SalirBus(View view){finish();}

    public void BuscarMapa(View view){
        Intent intent = new Intent(this,Mapas.class);
        startActivity(intent);
    }


}