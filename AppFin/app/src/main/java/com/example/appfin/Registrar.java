package com.example.appfin;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class Registrar extends AppCompatActivity {

    private TextView txtNombre;
    private TextView txtApellido;
    private TextView txtCorreo;
    private TextView txtClave;

        Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        txtNombre = (TextView) findViewById(R.id.TxtNombre);
        txtApellido = (TextView) findViewById(R.id.TxtApellido);
        txtCorreo = (TextView) findViewById(R.id.TxtCorreo);
        txtClave=(TextView) findViewById(R.id.TxtClave);
        Limpiar();
    }

    public void RegistrarUsuario(View view) {
        AdminSQL admin = new AdminSQL(this, "Entidades", null, 1);
        SQLiteDatabase DB = admin.getWritableDatabase();

        String nombre = txtNombre.getText().toString();
        String apellido = txtApellido.getText().toString();
        String correo = txtCorreo.getText().toString();
        String clave = txtClave.getText().toString();

        if (!nombre.isEmpty() && !apellido.isEmpty() && !correo.isEmpty() && !clave.isEmpty()) {
            ContentValues registro = new ContentValues();
            registro.put("correo", correo);
            registro.put("nombre", nombre);
            registro.put("apellido", apellido);
            registro.put("clave", clave);

            DB.insert("usuarios", null, registro);

            DB.close();

            Limpiar();
            AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
            dialogo.setTitle("Informacion");
            dialogo.setMessage("Registro guardado Exitosamente");

            dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialogo.show();

            Toast.makeText(this, "Registro Guardado", Toast.LENGTH_LONG).show();
            Limpiar();
        } else {
            Toast.makeText(this, "Los campos no pueden ser vacios", Toast.LENGTH_LONG).show();
        }

    }


    public void cancelar(View view){
        finish();
    }


    public void Limpiar(){
        txtNombre.setText("");
        txtApellido.setText("");
        txtCorreo.setText("");
        txtClave.setText("");
    }
}